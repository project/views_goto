// $Id$

This modul adds a new display type for views (drupal.org/project/views). It redirects the specified path to the node-page of the first item in the view's result.

The redirect is executed by calling drupal_goto(). The node is specified by fetching the ->nid from the first query result. @see views_goto_plugin_display_goto.inc at views_goto_plugin_display_goto::render()
